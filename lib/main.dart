import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController pesoController = TextEditingController();
  TextEditingController alturaController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _infoText = "Informe seu dados!";

  void _resetFields() {
    debugPrint("btn action refresh");
    pesoController.text = "";
    alturaController.text = "";

    setState(() {
      _infoText = "Informe seu dados!";
    });
  }

  void _calculate() {
    setState(() {
      double weight = double.parse(pesoController.text);
      double heigth = double.parse(alturaController.text) / 100;
      double imc = weight / (heigth * heigth);

      print(imc);
      if (imc < 18.6) {
        _infoText = "Abaixo do peso (${imc.toStringAsPrecision(3)})";
      } else if (imc >= 18.6 && imc < 24.9) {
        _infoText = "Peso Ideal (${imc.toStringAsPrecision(3)})";
      } else if (imc >= 24.9 && imc < 29.9) {
        _infoText = "Levemente acima do Peso (${imc.toStringAsPrecision(3)})";
      } else if (imc >= 29.9 && imc < 34.9) {
        _infoText = "Obesidade Grau I (${imc.toStringAsPrecision(3)})";
      } else if (imc >= 34.9 && imc < 39.9) {
        _infoText = "Obesidade Grau II (${imc.toStringAsPrecision(3)})";
      } else if (imc >= 40.0) {
        _infoText = "Obesidade Grau III (${imc.toStringAsPrecision(3)})";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mini Curso Fatec IMC"),
        centerTitle: true,
        backgroundColor: Colors.blueAccent,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.refresh), onPressed: _resetFields)
        ],
      ),
      backgroundColor: Colors.grey[50],
      body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Icon(Icons.person_pin, size: 120.0, color: Colors.blueAccent),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Peso",
                      labelStyle: TextStyle(color: Colors.blueAccent),
                      suffixIcon: Icon(Icons.add_box, color: Colors.green),
                      hintText: "kg"),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18.0, color: Colors.blue),
                  controller: pesoController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Informe seu peso!";
                    }
                  },
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Altura",
                      labelStyle: TextStyle(color: Colors.blueAccent),
                      suffixIcon: Icon(Icons.add_box, color: Colors.green),
                      hintText: "cm"),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18.0, color: Colors.blue),
                  controller: alturaController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Informe sua altura!";
                    }
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Container(
                    height: 50.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _calculate();
                        }
                      },
                      child: Text("Calcular",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w400)),
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
                Text(
                  _infoText,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blueAccent, fontSize: 20.0),
                ),
              ],
            ),
          )),
    );
  }
}
